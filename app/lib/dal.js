var mongoose = require('mongoose');

module.exports = function (config) {
  
    if (config.configName == "appfog")
    {
      var env = JSON.parse(process.env.VCAP_SERVICES);
      var obj = env['mongodb-1.8'][0]['credentials'];
    
      if(obj.username && obj.password){
        config.mongodb.uri = "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
      }else{
        config.mongodb.uri = "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
      }
    }
  
    //set up mongoose database connection
    if(!mongoose.connection.readyState){
      mongoose.connect(config.mongodb.uri);
    }
}